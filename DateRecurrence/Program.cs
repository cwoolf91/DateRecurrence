﻿using System;
using System.Collections;

namespace DateRecurrence
{
    internal class Program
    {
        private static void Main(string[] args)
        {
        }
    }

    [Serializable]
    public class DateOccurence
    {
        private DateTimeOffset CurrentDay;
        private DateTimeOffset StartDate;
        private DateTimeOffset EndDate;
        private ArrayList DateOccurences;
        private int NumberOfOccurences = 0;

        // Basic constructor
        public DateOccurence(DateTimeOffset StartDate, int NumOfOccurences)
        {
            SetCurrentDay();
            this.StartDate = StartDate;
            TimeSpan temp = TimeSpan.Zero;
            temp = TimeSpan.FromDays(NumberOfOccurences);
            DateTimeOffset tempOffset = new DateTimeOffset(StartDate.DateTime, temp);
            this.EndDate = tempOffset.DateTime;
            this.NumberOfOccurences = NumOfOccurences;
            this.DateOccurences = new ArrayList();
        }

        // Figure out the number of occurences without being specified.
        public DateOccurence(DateTimeOffset StartDate, DateTimeOffset EndDate)
        {
            SetCurrentDay();
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            TimeSpan temp = EndDate - StartDate;
            this.NumberOfOccurences = temp.Days;
            this.DateOccurences = new ArrayList();
        }

        // Just pass in a start date and add occurences forever. Might be really heavy on resources.
        public DateOccurence(DateTimeOffset StartDate)
        {
            SetCurrentDay();
            DateTime tempEnd = new DateTime(2019, 1, 1);
            this.StartDate = StartDate;
            this.EndDate = new DateTimeOffset(tempEnd);
            TimeSpan temp = EndDate - StartDate;
            this.NumberOfOccurences = temp.Days;
            this.DateOccurences = new ArrayList();
            PopulateDates();
        }

        // Helper function to get the current date time.
        private void SetCurrentDay()
        {
            this.CurrentDay = DateTimeOffset.UtcNow;
        }

        // Helper function to add to the array.
        public void AddNextOccurence(DateTimeOffset DateToAdd)
        {
            SetCurrentDay();

            // Start date must be the same date today and stop on or before the last day.
            if (DateTimeOffset.Compare(this.StartDate, DateToAdd) <= 0 && DateTimeOffset.Compare(DateToAdd, this.EndDate) <= 0)
            {
                DateOccurences.Add(DateToAdd);
                DateOccurences.TrimToSize();
                DateOccurences.Sort();
            }
        }

        // Can pass in an array of strings in date format.
        private void PopulateDates(String[] DatesToAdd)
        {
            DateTimeOffset temp;
            foreach (String str in DatesToAdd)
            {
                temp = DateTimeOffset.Parse(str);
                AddNextOccurence(temp);
            }
        }

        // Pass in a custom set of dates to add from an array of DateTime's.
        private void PopulateDates(DateTimeOffset[] DatesToAdd)
        {
            DateTimeOffset temp = this.StartDate;
            if (temp.CompareTo(DatesToAdd[0]) >= 0)
            {
                for (int i = 0; i < DatesToAdd.Length; i++)
                {
                    AddNextOccurence(temp);
                }
            }
        }

        private void PopulateDates()
        {
            // Vars...
            DateTimeOffset tempStart = this.StartDate;
            DateTimeOffset tempEnd = this.EndDate;
            int startHour = tempStart.Hour;
            int startMin = tempStart.Minute;
            int startSeconds = tempStart.Second;
            int endHour = tempEnd.Hour;
            int endMin = tempEnd.Minute;
            int endSeconds = tempEnd.Second;
            DateTimeOffset curr = this.StartDate;

            // Add all the occurences
            for (int i = 0; i < NumberOfOccurences; i++)
            {
                // This is in the last occurence is valid on the last day.
                if (DateTimeOffset.Compare(curr, tempEnd) == 0)
                {
                    // Must be at least one second LESS that ending date/time.
                    if (startSeconds < endSeconds && startMin <= endMin && startHour <= endHour)
                    {
                        AddNextOccurence(curr);
                        curr = curr.AddDays(1);
                    }
                }
                // Just add as long as we aren't on the last day.
                else if (DateTimeOffset.Compare(curr, tempEnd) < 0)
                {
                    AddNextOccurence(curr);
                    curr = curr.AddDays(1);
                }
            }
        }

        // Required method to return the next date time occurence.
        public DateTimeOffset GetNextRecurrence()
        {
            DateTimeOffset NextOccur;
            if (DateOccurences.Count > 0)
            {
                NextOccur = (DateTimeOffset)DateOccurences[0];
            }
            return NextOccur;
        }

        // Get next occurence after x amount of days.
        public DateTimeOffset GetNextRecurrence(int AfterNumOfDays)
        {
            DateTimeOffset NextOccur;
            if (DateOccurences.Count > 0 && DateOccurences.Count > AfterNumOfDays)
            {
                int temp = AfterNumOfDays + 1;
                NextOccur = (DateTimeOffset)DateOccurences[temp];
            }
            return NextOccur;
        }
    }
}