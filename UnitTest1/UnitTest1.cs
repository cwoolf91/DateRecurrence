using Microsoft.VisualStudio.TestTools.UnitTesting;
using DateRecurrence;
using System;

namespace UnitTest1
{
    [TestClass]
    public class UnitTest1
    {
        static DateTime template = new DateTime(2018, 11, 5);
        DateTimeOffset StartTest = DateTimeOffset.Now;
        DateTimeOffset EndTest = new DateTimeOffset(template);
        int oc = 5;

        [TestMethod]
        public void TestConstructor1()
        {
            DateTimeOffset StartTest = DateTimeOffset.Now;
            int oc = 5;
            DateOccurence dr = new DateOccurence(StartTest, oc);

            Assert.IsNotNull(dr);
        }

        [TestMethod]
        public void TestConstructor2()
        {
            DateTimeOffset StartTest = DateTimeOffset.Now;
            DateTimeOffset EndTest = new DateTimeOffset(template);
            DateOccurence dr = new DateOccurence(StartTest, EndTest);

            Assert.IsNotNull(dr);
        }

        [TestMethod]
        public void TestConstructor3()
        {
            DateTimeOffset StartTest = DateTimeOffset.Now;
            DateOccurence dr = new DateOccurence(StartTest);

            Assert.IsNotNull(dr);
        }

        [TestMethod]
        public void TestAddNewDate()
        {
            DateTime template = new DateTime(2018, 4, 5);
            DateTimeOffset NewDate = new DateTimeOffset(template);
            DateOccurence dr = new DateOccurence(StartTest, EndTest);

            dr.AddNextOccurence(NewDate);
        }

        [TestMethod]
        public void TestGetNextRecurrence1()
        {
            DateTime template = new DateTime(2018, 4, 5);
            DateTimeOffset NewDate = new DateTimeOffset(template);
            DateOccurence dr = new DateOccurence(StartTest, EndTest);

            dr.AddNextOccurence(NewDate);
            DateTimeOffset TestDate = dr.GetNextRecurrence();

            Assert.AreEqual(NewDate, TestDate.DateTime);
        }

        [TestMethod]
        public void TestGetNextRecurrence2()
        {
            DateTime template = new DateTime(2018, 4, 5);
            DateTimeOffset NewDate = new DateTimeOffset(template);
            DateOccurence dr = new DateOccurence(StartTest, EndTest);

            dr.AddNextOccurence(NewDate);
            DateTimeOffset TestDate = dr.GetNextRecurrence();

            Assert.AreEqual(NewDate, TestDate.DateTime);

            template = new DateTime(2018, 4, 3);
            DateTimeOffset NewDate2 = new DateTimeOffset(template);
            dr.AddNextOccurence(NewDate2);
            TestDate = dr.GetNextRecurrence();

            Assert.AreEqual(NewDate2, TestDate.DateTime);
        }

        [TestMethod]
        public void TestGetNextRecurrenceAfterDays()
        {
            DateTime template = new DateTime(2018, 4, 5);
            DateTimeOffset NewDate = new DateTimeOffset(template);
            DateOccurence dr = new DateOccurence(StartTest);

            dr.AddNextOccurence(NewDate);
            DateTimeOffset TestDate = dr.GetNextRecurrence();

            template = new DateTime(2018, 3, 20);
            DateTimeOffset NewDate2 = new DateTimeOffset(template);
            dr.AddNextOccurence(NewDate2);
            TestDate = dr.GetNextRecurrence(7);

            Assert.AreEqual(NewDate2, TestDate.DateTime);
        }
    }
}
